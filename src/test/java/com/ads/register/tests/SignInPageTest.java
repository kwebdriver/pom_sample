package com.ads.register.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ads.register.pages.HomePage;
import com.ads.register.pages.SignInPage;

public class SignInPageTest extends BaseTest {

	SignInPage signIn = null;
	HomePage homePage = null;
	
	@BeforeMethod
	public void initPages() {
		signIn = new SignInPage(driver);
		homePage = new HomePage(driver);
	
	}
	
	@Test(priority=0)
	public void validateSingInPage() {
		homePage.navigateSignInPage();
		Assert.assertEquals(getTitle(), signIn.pageTitle);
	}
	
	@Test(enabled=false)
	public void tc001_ValidateLoginWithEmptyCredentials() {
		
	}
	
	@Test(priority=1)
	public void tc002_ValidateLoginWithInvalidCredentials() {
		signIn.doLogin("karthik@gmail.com", "123456");
		Assert.assertTrue(signIn.txtFld_email.isDisplayed()); // element validation
		Assert.assertEquals(getTitle(), signIn.pageTitle); // title validation
		Assert.assertEquals(getText(signIn.label_errorMsg).trim(), signIn.errorMsg.trim());
	}
	
	@Test(enabled=false)
	public void tc002_ValidateLoginWithValidCredentials() {
		
	}
	

}
