package com.ads.register.tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ads.register.pages.HomePage;
import com.ads.register.pages.RegistrationPage;
import com.ads.register.pages.SignInPage;

public class HomePageTest extends BaseTest {

	HomePage homePage = null;
	RegistrationPage regPage = null;
	SignInPage signInPage = null;
			
	@BeforeMethod
	public void initPages() {
		homePage = new HomePage(driver);
		regPage = new RegistrationPage(driver);
		signInPage = new SignInPage(driver);
	}
	
	@AfterMethod
	public void navToHomePage() {
		navigateBack();
	}
	
	@Test(priority=1)
	public void validateNavRegPageFromHomePage() {
		homePage.navigateRegistrationPage();
		Assert.assertEquals(getTitle(), regPage.REG_PAGE_TITLE);
		Assert.assertTrue(regPage.txt_FirstName.isDisplayed());
	}
	
	@Test(priority=0)
	public void validateNavSignPageFromHomePage() {
		homePage.navigateSignInPage();
		Assert.assertEquals(getTitle(), signInPage.pageTitle);
		Assert.assertTrue(signInPage.txtFld_email.isDisplayed());
	}
	
	

}
