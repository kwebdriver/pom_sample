package com.ads.register.tests;

import org.testng.annotations.Test;

import com.ads.common.ConfigPropertiesReader;
import com.ads.common.IConstants;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class BaseTest {

	ConfigPropertiesReader propReader = new ConfigPropertiesReader();
	static WebDriver driver = null;
//	static String url = "http://demo.automationtesting.in/Index.html";

	@BeforeTest
	public void openBrowser() {
		propReader.readConfig();
		getBrowser(propReader.getBrowser());
		driver.get(propReader.getUrl());
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
	}
	
	public void getBrowser(String browser) {
		if(browser.equalsIgnoreCase("chrome")){
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+IConstants.CHROME_DRIVER_EXE_PATH);
			driver = new ChromeDriver();
		}
		if(browser.equalsIgnoreCase("firefox")){
			System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+IConstants.FF_DRIVER_EXE_PATH);
			driver = new FirefoxDriver();
		}
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}
	
	public void navigateBack() {
		driver.navigate().back();
	}
	
	public String getTitle() {
		return driver.getTitle();
	}

	public String getText(WebElement ele) {
		return ele.getText();
	}
	
	
	
	
}
