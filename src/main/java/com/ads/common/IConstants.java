package com.ads.common;

public interface IConstants {
	
	
	String CHROME_DRIVER_EXE_PATH = "\\src\\main\\resources\\exe\\windowsdriver\\chromedriver.exe";
	String FF_DRIVER_EXE_PATH = "\\src\\main\\resources\\exe\\windowsdriver\\geckodriver.exe";
	String SCREEN_SHOT_LOCATION = "\\test-output\\Screenshots\\";
	String ENVIRONMENT_PROPERTIES_PATH = "\\src\\test\\resources\\Configg\\env.properties";

}
