package com.ads.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigPropertiesReader {
	
	String browser = null;
	String url = null;

	
	public String getBrowser() {
		return browser;
	}
	
	public void setBrowser(String browser) {
		this.browser = browser;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public void readConfig() {
		
		String path = System.getProperty("user.dir")+"\\src\\test\\resources\\Configg\\env.properties";
		File file = null;
		FileReader fr = null;
		Properties props = new Properties();
		file = new File(path);
		try {
			fr= new FileReader(file);
			props.load(fr);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setUrl(props.getProperty("url"));
		setBrowser(props.getProperty("browser"));
	}
	

}
