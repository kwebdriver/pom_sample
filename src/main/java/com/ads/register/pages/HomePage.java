package com.ads.register.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class HomePage {

	WebDriver driver = null;

	public HomePage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public String HOME_PAGE_TITLE = "Index";

	@FindBy(id="email")
	public WebElement  txt_email;

	@FindBy(xpath="//img[@id='enterimg']")
	public WebElement button_enter;

	@FindBy(xpath="//button[@style='background-color:#0177b5']")
	public WebElement button_signin;

	@FindBy(xpath="//button[@id='btn2']")
	public WebElement button_skipsignin;

	public void navigateRegistrationPage() {
		txt_email.sendKeys("karthik@gmail.com");
		button_enter.click();
	}
	
	public void navigateSignInPage() {
		button_signin.click();
	}

}
