package com.ads.register.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignInPage {
	
	WebDriver driver = null;

	public SignInPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public String pageTitle = "SignIn";
	public String errorMsg = " Invalid User Name or PassWord ";
	
	@FindBy(xpath="(//input)[1]")
	public WebElement txtFld_email;
	
	@FindBy(xpath="(//input)[2]")
	public WebElement txtFld_password;
	
	@FindBy(id="enterbtn")
	public WebElement btn_enter;
	
	@FindBy(id="errormsg")
	public WebElement label_errorMsg;
	
	
	public void doLogin(String userName, String password) {
		txtFld_email.sendKeys(userName);
		txtFld_password.sendKeys(password);
		btn_enter.click();
	}
	
	
	

}
