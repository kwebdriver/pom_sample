package com.ads.register.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegistrationPage {
	

WebDriver driver = null;

public RegistrationPage(WebDriver driver){
	this.driver = driver;
	PageFactory.initElements(driver, this);
}
public String REG_PAGE_TITLE = "Register";
	
@FindBy(xpath="//input[@ng-model='FirstName']")
public WebElement txt_FirstName; 

@FindBy(xpath="//input[@ng-model='LastName']")
public WebElement txt_LastName;

@FindBy(xpath="//textarea[@class='form-control ng-pristine ng-untouched ng-valid']")
public WebElement txt_address;

@FindBy(xpath=" //input [@ng-model='EmailAdress']")
public WebElement txt_Email;

@FindBy(xpath="  //input [@type='tel']")
public WebElement txt_phnNo;

@FindBy(xpath=" //input [@type='radio'and @value='Male']")
public WebElement gendr_Male;

@FindBy(xpath=" //input [@type='radio'and @value='feMale']")
public WebElement gendr_female;

@FindBy(id="checkbox1")
public WebElement hobbies_cricket;

@FindBy(id="checkbox2")
public WebElement hobhies_movies;


@FindBy(id="checkbox3")
public WebElement hobbies_hockey;


@FindBy(id="msdd")
public WebElement  txt_language;


@FindBy(id="Skills")
public WebElement  slt_Skills;

@FindBy(xpath=" //select[@id='countries']")
public WebElement  slt_country;

@FindBy(xpath=" //span[@class='select2-selection select2-selection--single']")
public WebElement  slt_country2;

@FindBy(xpath=" //select[@id='yearbox']")
public WebElement  slt_year;


@FindBy(xpath="//select[@ng-model='monthbox']")
public WebElement  slt_month;

@FindBy(xpath="//select[@ng-model='daybox']")
public WebElement  slt_day;


@FindBy(id="firstpassword")
public WebElement  txt_pswd;



@FindBy(id="secondpassword")
public WebElement  txt_secpswd;

@FindBy(xpath=" //button[@id='submitbtn']")
public WebElement btn_submit;


@FindBy(xpath=" //button[@id='Button1']")
public WebElement btn_refresh;


}

